# TVRemote

## iOS Application

### How to Use
1. Simply swipe up, down, left, or right to send corresponding signals to TV
2. Swipe up or down on lower right rectangle to control volume.
3. Long press lower right rectangle to mute TV.

### Privacy Policy for iOS IOTVRemote

We do not collect any information within this application and as such we do not share any information from this application.

## Receiever

The receiver code here was tested on a ESP8266. The iOS Application IOTVRemote is used to send signals to the receiver which then broadcasts a signal using an infrared light to the TV.